# AKIRA SYSTEM STATUS

## Project Description
A very simple React-based, web app that shows Akira's current system status (business hours, whether we're currently open, etc.). The app is targeted at mobile devices with small screens — e.g. an iPhone or Android smartphone. It fetches the current system status as JSON from an API endpoint provided by Akira

This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!
