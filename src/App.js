//main component that holds and displays all the other components

import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import IsOpenForBusiness from './is_open_for_business';
import OpenTimes from './open_times';
import AnalogClock, { Themes } from './clock/index';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AppBar from 'material-ui/AppBar';

class App extends Component {

constructor (props){
    super();
    this.state = {
      system_status: {},
      open_at: "",
      close_at:""
    };
  }

  // this function fetches system status data from Akira's API
  fetchSytemData (url){
      fetch(url)
      .then(results => {
        return results.json()
      })
      .then(data => {
      this.setState({system_status: data})
      this.setState({open_at: data.open_hours_today.open_at})
      this.setState({close_at: data.open_hours_today.close_at})
    })
  }



componentDidMount(){
  this.fetchSytemData('https://app.akira.md/api/system_status')
}

  render() {
    return (
      <MuiThemeProvider>
      <div className="App">
         <AppBar
          style={{backgroundColor: 'white'}}
          iconElementLeft = {<img src={logo} className="App-logo" alt="logo" />}
        />
        < IsOpenForBusiness open_status={ this.state.system_status.is_open_for_business }/>
        <span className="clock"> <AnalogClock theme={Themes.aqua} width={200} /> </span>
        < OpenTimes open_at={this.state.open_at} close_at={this.state.close_at} />
      </div>
      </MuiThemeProvider>
    );
  }
}

export default App;
