import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

import AnalogClockLayout from './AnalogClockLayout';
import Styles from './styles';
import { cssTransform, updateTime } from './util';
import { light } from './themes';

export default class AnalogClock extends Component {

    constructor(props) {
        super();

        const date = this.initializeTime();
        //sets initial state to the time fetched from akira API
        this.state = {
            seconds: date[2],
            minutes: date[1],
            hour: date[0],
        };

        this.styles = cssTransform(Styles, props);
    }

    componentDidMount() {
        // update time every second interval
        this.interval = setInterval(() => {
            this.setState(updateTime(this.state));
        }, 1000);
    }

    componentWillReceiveProps(nextProps) {
        this.styles = cssTransform(Styles, nextProps);
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }

    //fetches the system time from akira API
    fetchCurrentTime (url){
      fetch(url)
      .then(results => {
        return results.json()
      })
      .then(data => {
      let current_system_time = data.system_time
      return moment(current_system_time).format("ddd MMM Do YYYY h:mm:ss")
    })
  }

    // to initialize the time to be displayed on the clock
    initializeTime() {
        const now =  this.fetchCurrentTime('https://app.akira.md/api/system_status') //new Date();
        return [moment(now).get('hour'), moment().get('minute'), moment().get('second')];
    }

    render() {
        return <AnalogClockLayout {...this.state} styles={this.styles} />;
    }
}

AnalogClock.propTypes = {
    theme: PropTypes.shape({
        background: PropTypes.string.isRequired,
        border: PropTypes.string.isRequired,
        center: PropTypes.string.isRequired,
        seconds: PropTypes.string.isRequired,
        minutes: PropTypes.string.isRequired,
        hour: PropTypes.string.isRequired,
        tick: PropTypes.string.isRequired,
    }),
    width: PropTypes.number,
    gmtOffset: PropTypes.string,
};

AnalogClock.defaultProps = {
    theme: light,
    width: 200,
};