//this component displays the opening and closing time of the day

import React, { Component } from 'react';
import moment from 'moment';


class OpenTimes extends Component {

  render() {
    let open_time = this.props.open_at;
    let close_time = this.props.close_at;
      return (<div className="App-intro" >
                <p> {moment(open_time).format("h:mm a")} </p>
                <p> to </p>
                <p> {moment(close_time).format("h:mm a")}  </p>
              </div>
            );
  }
}

export default OpenTimes;
