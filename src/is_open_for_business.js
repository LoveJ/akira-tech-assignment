//this component displays whether or not the business is open

import React, { Component } from 'react';


class IsOpenForBusiness extends Component {

  render() {
    if (this.props.open_status === true ){
      return (<div className="App-intro"> <p> We're Open! </p> </div>);
    } else {
      return (<div className="App-intro"> <p> We're Closed! </p> </div>);
    }
  }
}

export default IsOpenForBusiness;
